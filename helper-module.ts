import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ImageOrientationService,
  ImageMakerService,
  ImageSaveService,
  HttpService,
  FileDragService,
  DragService,
  //CredentialService
} from './index_helper';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ImageOrientationService,
    ImageMakerService,
    ImageSaveService,
    HttpService,
    FileDragService,
    DragService,
    //CredentialService
  ],
})
export class HelperModule { }
