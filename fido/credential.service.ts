import {
    CredentialRequestOptionsJSON,
    CredentialCreationOptionsJSON,
} from './credential.interface';
import {
    credentialRequestOptions,
    credentialCreationOptions,
    publicKeyCredentialWithAssertion,
    publicKeyCredentialWithAttestation,
} from './credential.object';
import {
    base64urlToBuffer,
    bufferToBase64url,
    convert,
} from './converter.service';

/**
 * ユーザー作成処理
 * リクエスト変換→認証器処理→レスポンス変換を行う
 * 
 * @param option 
 */
export function convertCredentialCreateOptions(
    option: CredentialCreationOptionsJSON
): Promise<any>
{
    const request = convert(
                base64urlToBuffer,
                credentialCreationOptions,
                option
            );

    return navigator.credentials.create(request)
        .then(credential => {
            return convert(
                bufferToBase64url,
                publicKeyCredentialWithAttestation,
                credential as PublicKeyCredential
            );
        });
}

/**
 * ユーザー認証処理
 * リクエスト変換→認証器→レスポンス変換を行う
 * 
 * @param option 
 */
export function convertCredentialRequestOptions(
    option: CredentialRequestOptionsJSON
): Promise<any>
{
    const request = convert(
                base64urlToBuffer,
                credentialRequestOptions,
                option
            );

    return navigator.credentials.get(request)
        .then(credential => {
            return convert(
                bufferToBase64url,
                publicKeyCredentialWithAssertion,
                credential as PublicKeyCredential
            );
        });
}

