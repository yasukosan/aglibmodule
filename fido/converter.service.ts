import {
    convert as convertValue,
    copy as copyValue,
    Schema, 
} from './credential.object';

export type Base64urlString = string;


/**
 * base64文字列をデコードしバイナリ文字列はArrayBufferに変換し返す
 * 
 * @param baseurl64String 
 * @return ArrayBuffer
 */
export function base64urlToBuffer(baseurl64String: string): ArrayBuffer {

    const padding = "==".slice(0, (4 - (baseurl64String.length % 4)) % 4);
    const base64String = baseurl64String.replace(/-/g, "+").replace(/_/g, "/") + padding;
    
    const str = atob(base64String);
    
    const buffer = new ArrayBuffer(str.length);
    const byteView = new Uint8Array(buffer);
    for (let i = 0; i < str.length; i++) {
        byteView[i] = str.charCodeAt(i);
    }
    return buffer;
}

export function bufferToBase64url(buffer: ArrayBuffer): Base64urlString {
    const byteView = new Uint8Array(buffer);
    let str = "";
    for (const charCode of byteView) {
        str += String.fromCharCode(charCode);
    }

    const base64String = btoa(str);
    
    const base64urlString = base64String.replace(/\+/g, "-").replace(/\//g, "_").replace(/=/g, "");
    return base64urlString;
}

export function convert<From, To>(conversionFn: (v: From) => To, schema: Schema, input: any): any {
    if (schema === copyValue) {
        return input;
    }
    if (schema === convertValue) {
        return conversionFn(input);
    }
    
    if (schema instanceof Array) {
        return input.map((v: any) => convert<From, To>(conversionFn, schema[0], v));
    }
    if (schema instanceof Object) {
        const output: any = {};
        for (const [key, schemaField] of Object.entries(schema)) {
        if (!(key in input)) {
            if (schemaField.required) {
            throw new Error(`Missing key: ${key}`);
            }
            continue;
        }
        if (input[key] == null) {
            output[key] = null;
            continue;
        }
        output[key] = convert<From, To>(conversionFn, schemaField.schema, input[key]);
        }
        return output;
    }
}