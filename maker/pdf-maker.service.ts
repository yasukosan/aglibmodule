import { Injectable } from '@angular/core';
// import { SAWARABI_PDF_FONTS } from './SawarabiPDFFonts';
import { NOTOSANS_PDF_FONTS } from './NotosansPDFFonts';

import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as vfsFonts from 'pdfmake/build/vfs_fonts.js';

@Injectable()
export class PdfMakerService {

    constructor(
        // private Pdfmake: any = new pdfMake
    ) {
        // フォント設定を行う
        pdfMake.fonts = {
            ipag: {
                normal: 'notosans_medium.ttf',
            }
        };
        pdfMake.vfs = NOTOSANS_PDF_FONTS;
    }

    /**
     * 本家createPdfのラッパー、デフォルトフォントを設定
     * @param docDefinition any
     */
    createPdf(docDefinition: any) {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        docDefinition.defaultStyle.font = 'ipag';

        // noinspection TypeScriptUnresolvedFunction
        return pdfMake.createPdf(docDefinition);
    }

    /**
     * レイアウト情報からPDF作成
     * ダウンロードイベントは発生しない
     * @param docDefinition any レイアウト情報
     * @return PDFデータ返却
     */
    testPdfMake(docDefinition: any): any {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        // docDefinition.defaultStyle.font = 'msgothic';
        return pdfMake.createPdf(docDefinition);
    }

    /**
     * レイアウト情報からPDF作成
     * ダウンロードイベントを発生させる
     * @param docDefinition any レイアウト情報
     */
    pdfMakeForIE(docDefinition: any) {
        docDefinition.defaultStyle = docDefinition.defaultStyle || {};
        // docDefinition.defaultStyle.font = 'msgothic';
        pdfMake.createPdf(docDefinition).download('optionalName.pdf');
    }

    // URLの取得にもラップをかける
    createPdfUrl(docDefinition: any): Promise<any> {

        // callbackが嫌なのでなんとなくPromiseで実装
        return new Promise((resolve) => {
            const pdfDocGenerator = this.createPdf(docDefinition)
                .then(() => {
                    pdfDocGenerator.getDataUrl().then(url => {
                        resolve(url);
                    });
                });
        });
      }
}

