import { Injectable } from "@angular/core";

@Injectable()
export class DragService {

    reader;
    catOn = false;
    pdfOn = false;

    pageLayout = [];

    fname = '';
    name = '';

    private onImage = '';
    private moveSwitch = false;
    private mouseStartingPointX = 0;
    private mouseStartingPointY = 0;
    private mouseMoveX = 0;
    private mouseMoveY = 0;
    private mouseMoveCheckX = 0;
    private mouseMoveCheckY = 0;

    private x = 0;
    private y = 0;
    private canvasBase: HTMLCanvasElement;
    private canvasImage: HTMLImageElement;
    private canvasWidth;
    private canvasHeight;
    private canvasRatio;

    private catW = 0;
    private catH = 0;

    constructor(
    ) {}

    public setImage(image: string): void {
        this.onImage = image;
    }

    /**
     * 読み込まれた画像の表示
     * @param event マウスイベント
     */
    public resultImage(target: string): Promise<HTMLImageElement> {
        // canvas要素の取得doctypeを指定しないとエラーになるので注意
        this.canvasBase = <HTMLCanvasElement> document.getElementById(target);
        const ctx = this.canvasBase.getContext('2d');
        ctx.imageSmoothingEnabled = true;
        /**
         * 画像追加処理
         */
        this.canvasImage = new Image();
        return new Promise((resolve) => {
            this.canvasImage.onload = (e) => {
                this.canvasWidth = 300;
                this.canvasRatio = this.canvasWidth / this.canvasImage.naturalWidth;
                this.canvasHeight = this.canvasImage.naturalHeight * this.canvasRatio;
                this.canvasBase.setAttribute('width', this.canvasWidth.toString());
                this.canvasBase.setAttribute('height', this.canvasHeight.toString());
                ctx.drawImage(this.canvasImage, 0, 0, this.canvasWidth, this.canvasHeight);
                this.setMouseEvent();
                resolve(this.canvasImage);
            };
        });

        this.canvasImage.src = this.onImage;
    }


    /**
     * リサイズ領域表示イベント登録
     *
     */
    private setMouseEvent(): void {
        this.canvasBase.addEventListener('mousedown', (e) => {
            this.editStart(e);
        }, false);
        this.canvasBase.addEventListener('mouseup', (e) => {
            this.editEnd(e);
        }, false);
        this.canvasBase.addEventListener('mousemove', (e) => {
            if (this.moveSwitch) {
                this.editAreaView(e);
            }
        }, false);
        this.canvasBase.addEventListener('touchstart', (e) => {
            e.preventDefault();
            this.editStart(e);
        });
        this.canvasBase.addEventListener('touchend', (e) => {
            e.stopPropagation();
            this.editEnd(e);
        });
        this.canvasBase.addEventListener('touchmove', (e) => {
            if (this.moveSwitch) {
                this.editAreaView(e);
            }
        });
    }
    /**
     * マウスイベント開始
     * イベント開始地点の座標から差分方式で
     * 移動地点の座標を計算
     * @param e object
     */
    private editStart(e): void {
        const rect = this.canvasBase.getBoundingClientRect();
        this.x = rect.left;
        this.y = rect.top;
        this.mouseStartingPointX = e.pageX - rect.left;
        this.mouseStartingPointY = e.pageY - rect.top;
        this.moveSwitch = true;
        this.catOn = true;
    }
    /**
     * マウスイベント終了処理
     * @param e object
     */
    private editEnd(e): void {
        console.log(this.mouseStartingPointX + ':' + this.mouseStartingPointY + ':' + this.mouseMoveX + ':' + this.mouseMoveY);
        this.moveSwitch = false;
    }
    /**
     * マウスの動きに合わせて座標計測
     * 一つ前の座標から差分計算で現在の座標への移動量を計測
     * キャンバスの内容を都度再描画する
     * @param e obejct
     */
    private editAreaView(e): void {
        const ctx = this.canvasBase.getContext('2d');
        ctx.clearRect( 0, 0, this.canvasWidth, this.canvasHeight);
        this.mouseMoveX = -((this.mouseStartingPointX + this.x) - e.pageX);
        this.mouseMoveY = -(this.mouseMoveX / 0.75);
        this.mouseMoveX = (this.mouseMoveX < 0) ? -(this.mouseMoveX) : this.mouseMoveX;
        this.mouseMoveY = (this.mouseMoveY < 0) ? -(this.mouseMoveY) : this.mouseMoveY;
        this.catW = this.mouseMoveX;
        this.catH = this.mouseMoveY;
        ctx.drawImage(this.canvasImage, 0, 0, this.canvasWidth, this.canvasHeight);
        ctx.fillRect(this.mouseStartingPointX, this.mouseStartingPointY, this.mouseMoveX, this.mouseMoveY);
        ctx.fillStyle = 'rgba(100, 100, 100, 0.5)';
    }
    /**
     * マウスイベント削除
     * angularに依存せずにイベントを管理しているので必ず
     * イベントを毎回破棄すること
     */
    private removeMouseEvent(): void {
        this.canvasBase.removeEventListener('mousedown', (e) => {});
        this.canvasBase.removeEventListener('mouseup',   (e) => {});
        this.canvasBase.removeEventListener('mousemove', (e) => {});
    }

    /**
     * 変数イベントの初期化
     * @param type String: リセットタイプ
     */
    public reset(type: string): void {
        if (type === 'last') {
            this.removeMouseEvent();
        } else if (type === 'first') {
            this.reader = null;
            this.canvasImage = null;
            this.canvasBase = null;
            this.canvasImage = null;
            this.mouseMoveCheckX = null;
            this.mouseMoveCheckY = null;
            this.catOn = false;
            this.moveSwitch = false;
        }
    }
}
