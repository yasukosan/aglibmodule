import { Injectable } from "@angular/core";

@Injectable()
export class FileDragService {

    private onDrag = false;

    private Reader: FileReader = null;
    private ReadCSV = null;
    private ReadEncode = {utf: false, sjis: true};

    /**
     *
     * @param event ドラッグイベント
     */
    public onDragOverHandler(event: DragEvent): void {
        this.onDrag = true;
    }
    public onDragLeaveHandler(event: DragEvent): void {
        this.onDrag = false;
    }
    /**
     * ファイルドロップイベント
     * @param event ドラッグされたファイル
     */
    public onDropHandler(event, type: string): FileDragService {
        let files;
        if (type === 'drag') {
            files = event.dataTransfer.files;
        } else if (type === 'select') {
            files = event.target.files;
        }

        this.Reader = new FileReader();

        // データタイプの判定
        if (!files[0] || files[0].type.indexOf('image/') > 0) {
        } else {
            const result = [];
            this.Reader.onloadend = (e) => {
                const body = this.Reader.result.toString().split('\n');
                for (let j = 0; j < body.length; j++) {
                    result[j] = body[j].split(',');
                }
                const jb = JSON.stringify(result);
                this.ReadCSV = JSON.parse(jb);
            };
            // const encode = (this.readCSVEncode.sjis) ? 'Shift_JIS' : 'UTF-8';
            // this.Reader.readAsText(files[0], encode);
        }
        return this;
    }

    public stopScreenEven(event): FileDragService {
        event.preventDefault();
        return this;
    }
    public startScreenEven(event): FileDragService {
        event.preventDefault();
        return this;
    }
}
