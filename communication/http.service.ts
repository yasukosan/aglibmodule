import { Injectable } from '@angular/core';
import {
    HttpClient, HttpHeaders, HttpParams
} from '@angular/common/http';

@Injectable()
export class HttpService {

    private CSRF_TOKEN = '';
    private httpOptions: any = {};
    private server: string = 'http://192.168.2.225:8001';
    private path: string = '';

    private responseStatus: any[] = [];
    private responseData: any;
    private responseCode = 200;

    /**
     * コンストラクタ. HttpClientService のインスタンスを生成する
     *
     * @param {Http} http Httpサービスを DI する
     * @memberof HttpClientService
     */
    constructor(
        private http: HttpClient,
    ) {
        // this.setAuthorization('my-auth-token');
    }

    public setServerURL(server: string): HttpService {
        this.server = server;
        return this;
    }

    /**
     * JSONヘッダーを作成
     * @return HttpService
     */
    public setJSONHeader(): HttpService {
        this.buildHeader({
            'Content-Type': 'application/json'
        });
        return this;
    }

    /**
     * CSRFトークンを付与したヘッダーを作成
     * @param csrf strgin CSRFトークン
     */
    public setCSRFHeader(csrf: string = ''): HttpService {
        this.buildHeader({
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': csrf
        });
        return this;
    }
    /**
     * Bearerトークンを付与したヘッダーを作成
     * @param token string Bearerトークン
     * @return HttpService
     */
    public setBearerHeader(token: string): HttpService {
        this.buildHeader({
            'Content-Type'  : 'application/json',
            'Authorization' : 'Bearer ' + token
        });
        return this;
    }
    /**
     * CSRFトークンとBearerトークンを付与したヘッダーを作成
     */
    public setCsrfAndBearerHeader(csrf: string, token: string): HttpService {
        this.buildHeader({
            'Content-Type'  : 'application/json',
            'Authorization' : 'Bearer ' + token,
            'X-CSRF-TOKEN': csrf
        });
        return this;
    }

    /**
     * カスタムヘッダーを作成し保持する
     * @param header object ヘッダー情報
     */
    private buildHeader(header: any = {}): void {
        this.httpOptions = {
            headers: new HttpHeaders(header),
            body: null
        };
    }

    public buildParams(params: object): HttpParams
    {
        let body = new HttpParams();
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                body = body.set(key, params[key]);
            }
        }
        return body;
    }

    /**
     * HTTP GET メソッドを実行する
     * (toPromise.then((res) =>{}) を利用する場合のコード)
     *
     * @returns {Promise<any[]>}
     * @memberof HttpClientService
     */
    public async get(path, options: object = null): Promise<any> {

        return this.http
            .get(
                this.toURL(path) + this.toParam(options),
                this.httpOptions
            )
            .toPromise()
            .then((response) => {
                return response;
            })
            .catch((response) => {
                this.errorHandler(response);
                return response;
            });
    }

    /**
     * POST通信でデータ送信
     * @param path string 送信パス
     * @param data object 送信データオブジェクト
     * @return Promise<any>
     */
    public async post(path, data: any): Promise<any> {

        return this.http
            .post(
                this.toURL(path),
                JSON.stringify(data),
                this.httpOptions )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }
    /**
     * PUT通信でデータ更新
     * @path string path:送信先パス
     * @param any data:送信データ
     * @returns {Promise<any[]>}
     */
    public async put(path: string, data: any): Promise<any> {

        return this.http
            .put(this.server + path,
                this.toJSONString(data),
                this.httpOptions
            )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }
    /**
     * DELETE通信でデータ削除
     * @param string path : 送信パス
     * @pram object options : 送信データ
     * @returns {Promise<any[]>}
     */
    public async delete(path: string, options: object = null): Promise<any> {

        return this.http
            .delete(
                this.toURL(path) + '/' + this.toParam(options),
                this.httpOptions
            )
            .toPromise()
            .then(response => {
                return response;
            })
            .catch(response => {
                return response;
            });
    }

    /**
     * URLパラメーターをget用に変換する
     * @param option URLパラメーター
     * @return string
     */
    private toParam(option: object): string {
        if (option === null) {
            return '';
        }

        let param = '?';

        for (const key in option) {
            if (option.hasOwnProperty(key)) {
                param += key + '=' + option[key];
            }
        }
        return param;
    }

    /**
     * URLにパスを足す
     * @param path string パス
     * @return string
     */
    private toURL(path: string): string {
        return this.server + path;
    }

    /**
     * JSONオブジェクト、連想配列をJSON文字列に変換
     * @param json object JSON|連想配列
     * @return string
     */
    private toJSONString(json: object): string {
        return JSON.stringify(json);
    }

    /**
     * REST-API 実行時のエラーハンドラ
     * (toPromise.then((res) =>{}) を利用する場合のコード)
     *
     * @private
     * @param {any} err エラー情報
     * @memberof HttpClientService
     */
    private errorHandler(err) {
        console.log('Error occured.', err);
        // return Promise.reject(err.message || err);
    }

    /**
     * Authorizatino に認証トークンを設定しする
     *
     * @param {string} token 認証トークン
     * @returns {void}
     * @memberof HttpClientService
     * @description
     * トークンを動的に設定できるようメソッド化している
     * Bearer トークンをヘッダに設定したい場合はこのメソッドを利用する
     */
    public setAuthorization(token: string = null): void {
        if (!token) {
            return;
        }
        const bearerToken: string = `Bearer ${token}`;
            this.httpOptions.headers = this.httpOptions.headers.set('Authorization', bearerToken);
    }
}
