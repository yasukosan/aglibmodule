import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  PdfMakerService,
  ListLayoutService, RecruiteLayoutService,
  TacksheetLayoutService, PrintLayoutService,
  TacksheetMakerService,
  PostcardMakerService, RecruiteMakerService,
} from './';


// Import Layout
import {
  RecruiteLayoutType1Service, RecruiteLayoutType2Service,
  RecruiteLayoutType3Service, RecruiteLayoutType4Service,
  RecruiteLayoutType5Service
} from './';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    PdfMakerService,
    ListLayoutService, RecruiteLayoutService,
    TacksheetLayoutService, PrintLayoutService,
    TacksheetMakerService, 
    PostcardMakerService, RecruiteMakerService,
    RecruiteLayoutType1Service, RecruiteLayoutType2Service,
    RecruiteLayoutType3Service, RecruiteLayoutType4Service,
    RecruiteLayoutType5Service,
  ],
})
export class PrintModule { }
