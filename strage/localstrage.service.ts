import { Injectable } from '@angular/core';

@Injectable()
export class LocalStrageService {
    /**
     *
     */
    constructor(

    ) {

    }

    /**
     * データ保存
     * @param key string キー
     * @param data string データ
     */
    public save(key, data): void {
        localStorage.setItem(key, data);
    }

    /**
     * JSONデータを保存
     * @param key string キー
     * @param obj string JSON
     */
    public saveToJson(key, obj): void {
        this.save(key, this.toJson(obj));
    }

    /**
     * データ取得（文字列データ）
     * @param key string キー
     */
    public load(key): string {
        return localStorage.getItem(key);
    }

    /**
     * データ取得（JSONデータ）
     * @param key string キー
     */
    public loadToJson(key): object {
        return this.fromJson(this.load(key));
    }

    /**
     * データ削除
     * @param key string キー
     */
    public delete(key): void {
        localStorage.removeItem(key);
    }

    /**
     * JSONデータを文字列に変換
     * @param obj JSON
     * @return string
     */
    private toJson(obj: Object): string {
        return JSON.stringify(obj);
    }

    /**
     * 文字列をJSONに変換
     * @param txt string 文字列
     * @return JSON
     */
    private fromJson(txt: string): object {
        return JSON.parse(txt);
    }
}
